<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Undangan;
use App\Jobs\ThanksEmailJob;

class UndanganController extends Controller
{
    public function addEmailAddresses(Request $request) {
        foreach ($request->emailAddresses as $email) {
            $undangan = new Undangan;
            $undangan->email = $email;
            $undangan->expired_at = $request->expiredAt;
            $undangan->save();
        }

        return true;
    }

    public function fetchData(Request $request) {
        $email = $request->input('email');
        $sha1 = $request->input('sha1');

        if ($sha1 === 'true') {
            return response()->json(sha1($email));
        } else {
            $undangan = Undangan::where('email', $email)->first();
            return response()->json($undangan);
        }
    }

    public function saveData(Request $request) {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required|max:255',
            'designer_favorit' => 'required|max:255'
        ]);

        $undangan = Undangan::where('email', $request->email)->first();

        $undangan->nama = $request->nama;
        $undangan->tgl_lahir = $request->tanggal_lahir;
        $undangan->jenis_kelamin = $request->jenis_kelamin;
        $undangan->designer_favorit = $request->designer_favorit;
        $undangan->status = 2;
        $undangan->save();
    }

    public function sendEmail(Request $request) {
        $email = $request->input('email');

        dispatch(new ThanksEmailJob($email))->delay(date("H:i:s", time()+3600));
        return true;
    }
}
