<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Undangan extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'nama',
        'tgl_lahir',
        'jenis_kelamin',
        'designer_favorit',
        'expired_at',
        'status'
    ];
}
