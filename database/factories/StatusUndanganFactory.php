<?php

namespace Database\Factories;

use App\Models\StatusUndangan;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatusUndanganFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StatusUndangan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
