<?php

namespace Database\Factories;

use App\Models\Undangan;
use Illuminate\Database\Eloquent\Factories\Factory;

class UndanganFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Undangan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail,
            'nama' => $this->faker->unique()->name,
            'tgl_lahir' => $this->faker->date,
            'jenis_kelamin' => 'pria',
            'designer_favorit' => 'Anna Sui,Gucci'
        ];
    }
}
