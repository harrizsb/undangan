<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUndangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('undangans', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('nama')->default('');
            $table->date('tgl_lahir')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('jenis_kelamin')->default('');
            $table->string('designer_favorit')->default('');
            $table->date('expired_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->unsignedBigInteger('status')->nullable()->default(1);
            $table->timestamps();


            $table->foreign('status')->references('id')->on('status_undangans')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('undangans');
    }
}
