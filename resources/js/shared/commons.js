export const DESIGNER_LIST = [
    "Anna Sui",
    "Anton Heunis",
    "Adidas by Y-3 Yohji Yamamoto",
    "Bally",
    "Belstaff",
    "Boss by Hugo Boss",
    "BOYY",
    "Céline",
    "Common Projects",
    "DKNY",
    "Emporio Armani",
    "Farm",
    "Gucci"
];

export const sendThanksEmail = async email => {
    return await axios.get("/api/save-data", {
        params: {
            email
        }
    });
};

export const saveInvitationData = async data => {
    return await axios.post("/api/save-data", data);
};

export const fetchInvitationData = async (email, sha1 = false) => {
    return await axios.get("/api/fetch-data", {
        params: {
            email,
            sha1
        }
    });
};

export const fetchSha1 = async email => {
    return await axios.get("/api/fetch-sha1", {
        params: {
            email
        }
    });
};

export const addEmailAdmin = async data => {
    try {
        return await axios.post("/api/add-email-addresses", data);
    } catch (err) {
        console.log(err);
    }
};
