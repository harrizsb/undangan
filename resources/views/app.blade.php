<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div id="app"><App email="{{ $admin ? null: $email }}"/></div>
    <script src="{{ $admin ? asset('js/admin.js' ) : asset('js/public.js') }}"></script>
</body>
</html>
