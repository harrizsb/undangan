<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UndanganController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/add-email-addresses', [UndanganController::class, 'addEmailAddresses']);

Route::post('/save-data', [UndanganController::class, 'saveData']);

Route::get('/fetch-data', [UndanganController::class, 'fetchData']);

Route::get('/send-email', [UndanganController::class, 'sendEmail']);
